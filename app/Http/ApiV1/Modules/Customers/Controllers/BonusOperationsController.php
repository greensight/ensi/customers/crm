<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\BonusOperations\CreateBonusOperationAction;
use App\Domain\Customers\Actions\BonusOperations\DeleteBonusOperationAction;
use App\Domain\Customers\Actions\BonusOperations\PatchBonusOperationAction;
use App\Http\ApiV1\Modules\Customers\Queries\BonusOperationsQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Requests\PatchBonusOperationRequest;
use App\Http\ApiV1\Modules\Customers\Resources\BonusOperationsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BonusOperationsController
{
    public function create(CreateBonusOperationRequest $request, CreateBonusOperationAction $action): BonusOperationsResource
    {
        return new BonusOperationsResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, BonusOperationsQuery $query): AnonymousResourceCollection
    {
        return BonusOperationsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, BonusOperationsQuery $query): BonusOperationsResource
    {
        return new BonusOperationsResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchBonusOperationRequest $request, PatchBonusOperationAction $action): BonusOperationsResource
    {
        return new BonusOperationsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteBonusOperationAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
