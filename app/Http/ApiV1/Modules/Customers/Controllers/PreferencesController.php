<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Http\ApiV1\Modules\Customers\Queries\PreferencesQuery;
use App\Http\ApiV1\Modules\Customers\Resources\PreferencesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PreferencesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, PreferencesQuery $query): AnonymousResourceCollection
    {
        return PreferencesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
