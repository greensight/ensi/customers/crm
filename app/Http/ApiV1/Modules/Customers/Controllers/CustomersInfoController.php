<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\CustomerInfo\CreateCustomerInfoAction;
use App\Domain\Customers\Actions\CustomerInfo\DeleteCustomerInfoAction;
use App\Domain\Customers\Actions\CustomerInfo\PatchCustomerInfoAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersInfoQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomerInfoRequest;
use App\Http\ApiV1\Modules\Customers\Requests\PatchCustomerInfoRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersInfoResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersInfoController
{
    public function create(CreateCustomerInfoRequest $request, CreateCustomerInfoAction $action): CustomersInfoResource
    {
        return new CustomersInfoResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CustomersInfoQuery $query): AnonymousResourceCollection
    {
        return CustomersInfoResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function get(int $id, CustomersInfoQuery $query): CustomersInfoResource
    {
        return new CustomersInfoResource($query->findOrFail($id));
    }

    public function patch(int $id, PatchCustomerInfoRequest $request, PatchCustomerInfoAction $action): CustomersInfoResource
    {
        return new CustomersInfoResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteCustomerInfoAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
