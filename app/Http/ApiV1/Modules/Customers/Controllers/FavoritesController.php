<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Favorites\CreateFavoritesAction;
use App\Domain\Customers\Actions\Favorites\DeleteFavoritesAction;
use App\Http\ApiV1\Modules\Customers\Queries\FavoritesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ClearCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\DeleteCustomerFavoritesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\FavoritesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FavoritesController
{
    public function massCreate(CreateCustomerFavoritesRequest $request, CreateFavoritesAction $action): Responsable
    {
        $action->execute($request->getCustomerId(), $request->getProductIds());

        return new EmptyResource();
    }

    public function massDelete(DeleteCustomerFavoritesRequest $request, DeleteFavoritesAction $action): Responsable
    {
        $action->execute($request->getCustomerId(), $request->getProductIds());

        return new EmptyResource();
    }

    public function clear(ClearCustomerFavoritesRequest $request, DeleteFavoritesAction $action): EmptyResource
    {
        $action->execute($request->getCustomerId());

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, FavoritesQuery $query): AnonymousResourceCollection
    {
        return FavoritesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
