<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\ProductSubscribers\ClearProductSubscribesAction;
use App\Domain\Customers\Actions\ProductSubscribers\CreateProductSubscribeAction;
use App\Domain\Customers\Actions\ProductSubscribers\DeleteProductFromProductSubscribesAction;
use App\Http\ApiV1\Modules\Customers\Queries\ProductSubscribesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\ClearProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Requests\CreateProductSubscribeRequest;
use App\Http\ApiV1\Modules\Customers\Requests\DeleteProductFromProductSubscribesRequest;
use App\Http\ApiV1\Modules\Customers\Resources\ProductSubscribesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductSubscribesController
{
    public function create(CreateProductSubscribeRequest $request, CreateProductSubscribeAction $action): ProductSubscribesResource
    {
        return new ProductSubscribesResource($action->execute($request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, ProductSubscribesQuery $query): AnonymousResourceCollection
    {
        return ProductSubscribesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function deleteProduct(DeleteProductFromProductSubscribesRequest $request, DeleteProductFromProductSubscribesAction $action): EmptyResource
    {
        $action->execute($request->getCustomerId(), $request->getProductId());

        return new EmptyResource();
    }

    public function clear(ClearProductSubscribesRequest $request, ClearProductSubscribesAction $action): EmptyResource
    {
        $action->execute($request->getCustomerId());

        return new EmptyResource();
    }
}
