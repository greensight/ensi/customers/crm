<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\BonusOperation;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class BonusOperationsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(BonusOperation::query());

        $this->allowedSorts(['id', 'created_at', 'expiration_date', 'activation_date']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
            AllowedFilter::exact('order_number'),
            AllowedFilter::scope('earning'),
            AllowedFilter::scope('spending'),
        ]);

        $this->defaultSort('id');
    }
}
