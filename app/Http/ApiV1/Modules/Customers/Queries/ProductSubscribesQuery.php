<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\ProductSubscribe;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductSubscribesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(ProductSubscribe::query());

        $this->allowedSorts(['id', 'created_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
            AllowedFilter::exact('product_id'),
        ]);

        $this->defaultSort('id');
    }
}
