<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Preference;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class PreferencesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Preference::query());

        $this->allowedSorts(['id', 'product_count', 'product_sum', 'created_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
            AllowedFilter::exact('attribute_name'),
            AllowedFilter::exact('attribute_value'),
        ]);

        $this->defaultSort('id');
    }
}
