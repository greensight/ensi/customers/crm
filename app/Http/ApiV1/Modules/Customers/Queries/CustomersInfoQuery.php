<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\CustomerInfo;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CustomersInfoQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(CustomerInfo::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),
        ]);

        $this->defaultSort('id');
    }
}
