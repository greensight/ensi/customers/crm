<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class UpdateFavoritesRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'product_ids' => $this->faker->randomList(fn () => $this->faker->modelId(), min: 1),
        ];
    }

    public function fastMake(int $customerId, array $productIds): array
    {
        return $this->make([
            'customer_id' => $customerId,
            'product_ids' => $productIds,
        ]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
