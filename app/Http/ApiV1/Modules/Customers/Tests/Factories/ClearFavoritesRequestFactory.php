<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class ClearFavoritesRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
        ];
    }

    public function fastMake(int $customerId): array
    {
        return $this->make([
            'customer_id' => $customerId,
        ]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
