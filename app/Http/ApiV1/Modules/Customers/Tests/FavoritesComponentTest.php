<?php

use App\Domain\Customers\Models\Favorite;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\ClearFavoritesRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\UpdateFavoritesRequestFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/favorites:search 200', function () {
    postJson('/api/v1/customers/favorites:search')
        ->assertStatus(200);
});

test('POST /api/v1/customers/favorites:add-products 200', function () {
    $customerId = 1;
    $productIds = [2, 3];

    $request = UpdateFavoritesRequestFactory::new()->fastMake($customerId, $productIds);
    postJson('/api/v1/customers/favorites:add-products', $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseHas(Favorite::class, [
            'customer_id' => $customerId,
            'product_id' => $productId,
        ]);
    }
});

test('POST /api/v1/customers/favorites:add-products 400', function () {
    $customerId = 1;
    $productIds = [2, 3];

    Favorite::factory()->fastCreate($customerId, current($productIds));

    $request = UpdateFavoritesRequestFactory::new()->fastMake($customerId, $productIds);

    postJson('/api/v1/customers/favorites:add-products', $request)
        ->assertStatus(400);
});

test('POST /api/v1/customers/favorites:delete-products 200', function () {
    $customerId = 1;
    $productIds = [2, 3];
    $otherProductId = 4;
    foreach ($productIds as $productId) {
        Favorite::factory()->fastCreate($customerId, $productId);
    }
    Favorite::factory()->fastCreate($customerId, 4);

    $request = UpdateFavoritesRequestFactory::new()->fastMake($customerId, array_merge($productIds, [5]));


    postJson('/api/v1/customers/favorites:delete-products', $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseMissing(Favorite::class, [
            'customer_id' => $customerId,
            'product_id' => $productId,
        ]);
    }

    assertDatabaseHas(Favorite::class, [
        'customer_id' => $customerId,
        'product_id' => $otherProductId,
    ]);
});

test('POST /api/v1/customers/favorites:clear 200', function () {
    $customerId = 1;
    $otherCustomerId = 2;
    $productIds = [2, 3];
    foreach ($productIds as $productId) {
        Favorite::factory()->fastCreate($customerId, $productId);
        Favorite::factory()->fastCreate($otherCustomerId, $productId);
    }

    $request = ClearFavoritesRequestFactory::new()->fastMake($customerId);

    postJson('/api/v1/customers/favorites:clear', $request)
        ->assertStatus(200);

    foreach ($productIds as $productId) {
        assertDatabaseMissing(Favorite::class, [
            'customer_id' => $customerId,
            'product_id' => $productId,
        ]);
    }

    foreach ($productIds as $productId) {
        assertDatabaseHas(Favorite::class, [
            'customer_id' => $otherCustomerId,
            'product_id' => $productId,
        ]);
    }
});
