<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\BonusOperation;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin BonusOperation */
class BonusOperationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'order_number' => $this->order_number,
            'bonus_amount' => $this->bonus_amount,
            'comment' => $this->comment,
            'activation_date' => $this->activation_date,
            'expiration_date' => $this->expiration_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
