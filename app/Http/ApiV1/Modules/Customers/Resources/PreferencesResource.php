<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Preference;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Preference */
class PreferencesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'attribute_name' => $this->attribute_name,
            'attribute_value' => $this->attribute_value,
            'product_count' => $this->product_count,
            'product_sum' => $this->product_sum,
        ];
    }
}
