<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Favorite;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Favorite */
class FavoritesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'product_id' => $this->product_id,
        ];
    }
}
