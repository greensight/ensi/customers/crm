<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\CustomerInfo;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin CustomerInfo */
class CustomersInfoResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'kpi_sku_count' => $this->kpi_sku_count,
            'kpi_sku_price_sum' => $this->kpi_sku_price_sum,
            'kpi_order_count' => $this->kpi_order_count,
            'kpi_shipment_count' => $this->kpi_shipment_count,
            'kpi_delivered_count' => $this->kpi_delivered_count,
            'kpi_delivered_sum' => $this->kpi_delivered_sum,
            'kpi_refunded_count' => $this->kpi_refunded_count,
            'kpi_refunded_sum' => $this->kpi_refunded_sum,
            'kpi_canceled_count' => $this->kpi_canceled_count,
            'kpi_canceled_sum' => $this->kpi_canceled_sum,
        ];
    }
}
