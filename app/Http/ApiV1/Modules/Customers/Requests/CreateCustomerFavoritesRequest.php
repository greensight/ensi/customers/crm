<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Domain\Customers\Models\Favorite;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateCustomerFavoritesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $customerId = $this->getCustomerId();

        return [
            'customer_id' => ['required', 'integer'],
            'product_ids' => ['required', 'array', 'min:1'],
            'product_ids.*' => [
                'required',
                'integer',
                'distinct',
                Rule::unique(Favorite::class, 'product_id')->where('customer_id', "{$customerId}"),
            ],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->integer('customer_id');
    }

    public function getProductIds(): array
    {
        return $this->input('product_ids');
    }
}
