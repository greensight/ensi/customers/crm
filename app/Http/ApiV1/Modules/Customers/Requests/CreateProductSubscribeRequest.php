<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Domain\Customers\Models\ProductSubscribe;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateProductSubscribeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $customerId = (int)$this->input('customer_id');

        return [
            'customer_id' => ['required', 'integer'],
            'product_id' => [
                'required',
                'integer',
                Rule::unique(ProductSubscribe::class)->where('customer_id', "{$customerId}"),
            ],
        ];
    }
}
