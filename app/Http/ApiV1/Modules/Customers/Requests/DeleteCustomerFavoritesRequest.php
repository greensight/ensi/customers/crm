<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteCustomerFavoritesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'product_ids' => ['required', 'array', 'min:1'],
            'product_ids.*' => ['required', 'integer', 'distinct'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->integer('customer_id');
    }

    public function getProductIds(): array
    {
        return $this->input('product_ids');
    }
}
