<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateBonusOperationRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'order_number' => ['nullable', 'integer'],
            'bonus_amount' => ['required', 'numeric'],
            'comment' => ['nullable', 'string'],
            'activation_date' => ['nullable', 'date'],
            'expiration_date' => ['nullable', 'date'],
        ];
    }
}
