<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class DeleteProductFromProductSubscribesRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', 'integer'],
            'product_id' => ['required', 'integer'],
        ];
    }

    public function getCustomerId(): int
    {
        return $this->input('customer_id');
    }

    public function getProductId(): int
    {
        return $this->input('product_id');
    }
}
