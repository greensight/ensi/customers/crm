<?php

namespace App\Console\Commands\Preferences;

use App\Domain\Customers\Actions\SyncPreferenceAction;
use Illuminate\Console\Command;
use Throwable;

class SyncPreferencesCommand extends Command
{
    protected $signature = 'preferences:sync';
    protected $description = 'Command to synchronise preferences according to configuration';

    public function handle(SyncPreferenceAction $action): void
    {
        try {
            $action->execute();
        } catch (Throwable $e) {
            logger()->error($e->getMessage(), $e->getTrace());
            $this->output->error($e->getMessage());
        }
    }
}
