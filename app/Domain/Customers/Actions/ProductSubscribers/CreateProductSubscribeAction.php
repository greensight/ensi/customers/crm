<?php

namespace App\Domain\Customers\Actions\ProductSubscribers;

use App\Domain\Customers\Models\ProductSubscribe;

class CreateProductSubscribeAction
{
    public function execute(array $fields): ProductSubscribe
    {
        $productSubscribe = new ProductSubscribe();
        $productSubscribe->fill($fields);
        $productSubscribe->save();

        return $productSubscribe;
    }
}
