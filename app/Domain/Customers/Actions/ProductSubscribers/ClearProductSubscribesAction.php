<?php

namespace App\Domain\Customers\Actions\ProductSubscribers;

use App\Domain\Customers\Models\ProductSubscribe;

class ClearProductSubscribesAction
{
    public function execute(int $customerId): void
    {
        ProductSubscribe::query()
            ->where('customer_id', $customerId)
            ->delete();
    }
}
