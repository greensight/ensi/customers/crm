<?php

namespace App\Domain\Customers\Actions;

use App\Domain\Customers\Models\Preference;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\ApiException as CustomerApiException;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\ApiException as OmsApiException;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\ApiException as PimApiException;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Generator;
use Illuminate\Support\Collection;

class SyncPreferenceAction
{
    public function __construct(
        protected ProductsApi  $productsApi,
        protected OrdersApi    $ordersApi,
        protected CustomersApi $customersApi,
        protected Collection $updatedPreference,
    ) {
    }

    /**
     * @throws OmsApiException
     * @throws PimApiException
     * @throws CustomerApiException
     */
    public function execute(): void
    {
        $this->updatedPreference = collect();
        $attributes = config('preferences');

        foreach ($this->collectCustomersProducts() as $customerOrders) {
            $products = $this->collectProductsInfo(
                array_unique($customerOrders->reduce(fn ($carry, $item) => array_merge($carry, $item->keys()->toArray()), [])),
                array_keys($attributes['including_attributes'])
            );

            /**
             * @var int $customerId
             * @var Collection $customerProductAggregate
             */
            foreach ($customerOrders as $customerId => $customerProductAggregate) {
                $customerProducts = $products->filter(
                    fn (Product $product) => in_array($product->getId(), $customerProductAggregate->keys()->all())
                );
                foreach ($attributes['including_attributes'] as $attribute => $field) {
                    $this->createOrUpdatePreferences(
                        $customerProductAggregate,
                        $customerProducts,
                        $customerId,
                        $attribute,
                        $field
                    );
                }
                foreach ($attributes['attributes'] as $attribute) {
                    $this->createOrUpdatePreferences(
                        $customerProductAggregate,
                        $customerProducts,
                        $customerId,
                        $attribute
                    );
                }
            }
        }

        Preference::query()->whereNotIn('id', $this->updatedPreference->unique()->all())->delete();
    }

    /**
     * Загружает информацию о покупателях и их заказах в удобной структуре для дальнейшей обработки
     * @throws OmsApiException
     * @throws CustomerApiException
     */
    protected function collectCustomersProducts(): Generator
    {
        do {
            $customers = $this->customersApi->searchCustomers(new SearchCustomersRequest([
                'pagination' => new RequestBodyPagination([
                    'type' => PaginationTypeEnum::CURSOR->value,
                    'cursor' => $customerCursor ?? null,
                ]),
                'filter' => ['active' => true],
            ]));
            $customerCursor = $customers->getMeta()->getPagination()->getNextCursor();

            $clientProducts = collect();
            do {
                $orders = $this->ordersApi->searchOrders(new SearchOrdersRequest([
                    'pagination' => new RequestBodyPagination([
                        'type' => PaginationTypeEnum::CURSOR->value,
                        'cursor' => $orderCursor ?? null,
                    ]),
                    'include' => ['items'],
                    'filter' => (object)[
                        'status' => OrderStatusEnum::DONE,
                        'is_return' => false,
                        'customer_id' => collect($customers->getData())->map->getId()->all(),
                    ],
                ]));
                foreach ($orders->getData() as $order) {
                    foreach ($order->getItems() as $item) {
                        /** @var Collection $customer */
                        if ($customer = $clientProducts->get($order->getCustomerId())) {
                            if ($existItem = $customer->get($item->getOfferId())) {
                                $customer[$item->getOfferId()] = [
                                    'product_count' => $existItem['product_count'] + $item->getQty(),
                                    'product_sum' => $existItem['product_sum'] + $item->getPrice(),
                                ];
                            } else {
                                $customer[$item->getOfferId()] = [
                                    'product_count' => $item->getQty(),
                                    'product_sum' => $item->getPrice(),
                                ];
                            }
                            $clientProducts[$order->getCustomerId()] = $customer;
                        } else {
                            $clientProducts[$order->getCustomerId()] = collect([
                                $item->getOfferId() => [
                                    'product_count' => $item->getQty(),
                                    'product_sum' => $item->getPrice(),
                                ],
                            ]);
                        }
                    }
                }
                $orderCursor = $orders->getMeta()->getPagination()->getNextCursor();
            } while (!is_null($orderCursor));

            yield $clientProducts;
        } while ($customerCursor ?? null);
    }

    /**
     * @throws PimApiException
     */
    protected function collectProductsInfo(array $productIds, array $include = []): Collection
    {
        $products = collect();
        do {
            $response = $this->productsApi->searchProducts(new SearchProductsRequest([
                'pagination' => new RequestBodyPagination(['type' => PaginationTypeEnum::CURSOR->value, 'cursor' => $cursor ?? null]),
                'include' => $include,
                'filter' => (object)['id' => $productIds],
            ]));
            $cursor = $response->getMeta()->getPagination()->getNextCursor();

            $products = $products->concat($response->getData());
        } while (!is_null($cursor));

        return $products;
    }

    protected function createOrUpdatePreferences(
        Collection $customerProductAggregate,
        Collection $customerProducts,
        $customerId,
        $attribute,
        $field = null
    ): void {
        $customerProducts->groupBy(is_null($field) ? $attribute : "$attribute.$field")
            ->each(fn (Collection $productGroup, $value) => $this->updatedPreference->push(
                Preference::query()->updateOrCreate(
                    [
                        'customer_id' => (int)$customerId,
                        'attribute_name' => (string)$attribute,
                        'attribute_value' => (string)$value,
                    ],
                    [
                        'product_count' => (float)$productGroup->reduce(
                            fn ($carry, Product $product) => $carry + $customerProductAggregate->get($product->getId())['product_count'],
                            0
                        ),
                        'product_sum' => (int)$productGroup->reduce(
                            fn ($carry, Product $product) => $carry + $customerProductAggregate->get($product->getId())['product_sum'],
                            0
                        ),
                    ]
                )->id
            ));
    }
}
