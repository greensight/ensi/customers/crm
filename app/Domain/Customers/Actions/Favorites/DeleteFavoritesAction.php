<?php

namespace App\Domain\Customers\Actions\Favorites;

use App\Domain\Customers\Models\Favorite;
use Illuminate\Support\Facades\DB;

class DeleteFavoritesAction
{
    public function execute(?int $customerId = null, ?array $productIds = null): void
    {
        $favoriteQuery = Favorite::query();

        if ($customerId) {
            $favoriteQuery->where('customer_id', $customerId);
        }

        if ($productIds) {
            $favoriteQuery->whereIn('product_id', $productIds);
        }

        $favorites = $favoriteQuery->get();

        if ($favorites->isEmpty()) {
            return;
        }

        DB::transaction(function () use ($favorites) {
            $favorites->each(fn (Favorite $favorite) => $favorite->delete());
        });
    }
}
