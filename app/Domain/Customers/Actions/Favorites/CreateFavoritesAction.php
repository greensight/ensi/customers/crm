<?php

namespace App\Domain\Customers\Actions\Favorites;

use App\Domain\Customers\Models\Favorite;
use Illuminate\Support\Facades\DB;

class CreateFavoritesAction
{
    public function execute(int $customerId, array $productIds): void
    {
        DB::transaction(function () use ($customerId, $productIds) {
            foreach ($productIds as $productId) {
                $this->makeFavorite($customerId, $productId);
            }
        });
    }

    public function makeFavorite(int $customerId, int $productId): void
    {
        $favorite = new Favorite();
        $favorite->customer_id = $customerId;
        $favorite->product_id = $productId;
        $favorite->save();
    }
}
