<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use App\Domain\Customers\Models\BonusOperation;

class DeleteBonusOperationAction
{
    public function execute(int $id): void
    {
        BonusOperation::destroy($id);
    }
}
