<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use App\Domain\Customers\Models\BonusOperation;

class CreateBonusOperationAction
{
    public function execute(array $fields): BonusOperation
    {
        $bonusOperation = new BonusOperation();
        $bonusOperation->fill($fields);
        $bonusOperation->save();

        return $bonusOperation;
    }
}
