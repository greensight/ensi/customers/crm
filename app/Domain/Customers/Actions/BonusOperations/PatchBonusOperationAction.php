<?php

namespace App\Domain\Customers\Actions\BonusOperations;

use App\Domain\Customers\Models\BonusOperation;

class PatchBonusOperationAction
{
    public function execute(int $id, array $fields): BonusOperation
    {
        /** @var BonusOperation $bonusOperation */
        $bonusOperation = BonusOperation::query()->findOrFail($id);
        $bonusOperation->update($fields);

        return $bonusOperation;
    }
}
