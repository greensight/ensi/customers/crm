<?php

namespace App\Domain\Customers\Actions\CustomerInfo;

use App\Domain\Customers\Models\CustomerInfo;

class DeleteCustomerInfoAction
{
    public function execute(int $id): void
    {
        CustomerInfo::destroy($id);
    }
}
