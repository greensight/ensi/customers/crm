<?php

namespace App\Domain\Customers\Actions\CustomerInfo;

use App\Domain\Customers\Models\CustomerInfo;

class PatchCustomerInfoAction
{
    public function execute(int $id, array $fields): CustomerInfo
    {
        /** @var CustomerInfo $customer */
        $customer = CustomerInfo::query()->findOrFail($id);
        $customer->update($fields);

        return $customer;
    }
}
