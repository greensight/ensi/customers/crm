<?php

namespace App\Domain\Customers\Actions\CustomerInfo;

use App\Domain\Customers\Models\CustomerInfo;

class CreateCustomerInfoAction
{
    public function execute(array $fields): CustomerInfo
    {
        $customerInfo = new CustomerInfo();
        $customerInfo->fill($fields);
        $customerInfo->save();

        return $customerInfo;
    }
}
