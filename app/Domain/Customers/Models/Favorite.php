<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\FavoriteFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Список избранного"
 *
 * @property int $id
 * @property int $customer_id
 * @property int $product_id
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property CustomerInfo $customer
 */
class Favorite extends Model
{
    protected $table = 'customer_favorites';

    protected $fillable = ['customer_id', 'product_id'];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(CustomerInfo::class);
    }

    public static function factory(): FavoriteFactory
    {
        return FavoriteFactory::new();
    }
}
