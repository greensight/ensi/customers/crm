<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\BonusOperationFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "История начислений/списаний"
 *
 * @property int $id
 * @property int $customer_id               - id пользователя
 * @property int|null $order_number         - Номер заказа
 * @property float $bonus_amount            - Количество начисленных/списанных бонусов
 * @property string|null $comment           - Комментарий
 * @property CarbonInterface|null $activation_date   - Количество начисленных/списанных бонусов
 * @property CarbonInterface|null $expiration_date   - Количество начисленных/списанных бонусов
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class BonusOperation extends Model
{
    protected $table = 'bonus_operations';

    protected $fillable = ['customer_id', 'order_number', 'bonus_amount', 'comment', 'activation_date', 'expiration_date'];

    /** scope для отбора "Начисленных" баллов */
    public function scopeEarning(Builder $query): Builder
    {
        return $query->where('bonus_amount', '>=', 0);
    }

    /** scope для отбора "Списанных" баллов */
    public function scopeSpending(Builder $query): Builder
    {
        return $query->where('bonus_amount', '<', 0);
    }

    public static function factory(): BonusOperationFactory
    {
        return BonusOperationFactory::new();
    }
}
