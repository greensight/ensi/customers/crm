<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\PreferenceFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Информация о предпочтениях"
 *
 * @property int $id
 * @property int $customer_id           - id пользователя
 * @property string $attribute_name     - Название аттрибута агрегации
 * @property string $attribute_value    - Значение аттрибута агрегации
 * @property float $product_count       - количество купленного товара
 * @property int $product_sum           - сумма затраченная на товары
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class Preference extends Model
{
    protected $table = 'preferences';

    protected $fillable = ['customer_id', 'attribute_name', 'attribute_value', 'product_count', 'product_sum'];

    public static function factory(): PreferenceFactory
    {
        return PreferenceFactory::new();
    }
}
