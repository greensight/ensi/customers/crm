<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\BonusOperation;
use Ensi\LaravelTestFactories\BaseModelFactory;

class BonusOperationFactory extends BaseModelFactory
{
    protected $model = BonusOperation::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'order_number' => $this->faker->nullable()->randomNumber(),
            'bonus_amount' => $this->faker->randomFloat(),
            'comment' => $this->faker->nullable()->text(),
            'activation_date' => $this->faker->nullable()->date(),
            'expiration_date' => $this->faker->nullable()->date(),
        ];
    }
}
