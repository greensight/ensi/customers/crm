<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Preference;
use Ensi\LaravelTestFactories\BaseModelFactory;

class PreferenceFactory extends BaseModelFactory
{
    protected $model = Preference::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'attribute_name' => $this->faker->text(),
            'attribute_value' => $this->faker->text(),
            'product_count' => $this->faker->randomNumber(),
            'product_sum' => $this->faker->randomNumber(),
        ];
    }
}
