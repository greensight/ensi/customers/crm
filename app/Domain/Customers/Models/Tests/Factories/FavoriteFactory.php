<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Favorite;
use Ensi\LaravelTestFactories\BaseModelFactory;

/**
 * @extends BaseModelFactory<Favorite>
 */
class FavoriteFactory extends BaseModelFactory
{
    protected $model = Favorite::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }

    public function fastCreate(int $customerId, int $productId): Favorite
    {
        return $this->create([
            'customer_id' => $customerId,
            'product_id' => $productId,
        ]);
    }
}
