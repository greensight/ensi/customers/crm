<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\CustomerInfo;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CustomerInfoFactory extends BaseModelFactory
{
    protected $model = CustomerInfo::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'kpi_sku_count' => $this->faker->nullable()->randomNumber(),
            'kpi_sku_price_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_order_count' => $this->faker->nullable()->randomNumber(),
            'kpi_shipment_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_count' => $this->faker->nullable()->randomNumber(),
            'kpi_delivered_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_count' => $this->faker->nullable()->randomNumber(),
            'kpi_refunded_sum' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_count' => $this->faker->nullable()->randomNumber(),
            'kpi_canceled_sum' => $this->faker->nullable()->randomNumber(),
        ];
    }
}
