<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\ProductSubscribe;
use Ensi\LaravelTestFactories\BaseModelFactory;

class ProductSubscribeFactory extends BaseModelFactory
{
    protected $model = ProductSubscribe::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }
}
