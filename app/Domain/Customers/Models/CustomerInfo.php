<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\CustomerInfoFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Информация покупателя"
 *
 * @property int $id
 * @property int $customer_id              - ид покупателя
 * @property int|null $kpi_sku_count        - куплено товаров количество
 * @property int|null $kpi_sku_price_sum    - куплено товаров сумма
 * @property int|null $kpi_order_count      - количество заказов
 * @property int|null $kpi_shipment_count   - количество отправок
 * @property int|null $kpi_delivered_count  - доставлено количество
 * @property int|null $kpi_delivered_sum    - доставлено сумма
 * @property int|null $kpi_refunded_count   - возвращено количество
 * @property int|null $kpi_refunded_sum     - возвращено сумма
 * @property int|null $kpi_canceled_count   - отменено количество
 * @property int|null $kpi_canceled_sum     - отменено сумма
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property Collection|Favorite[] $favorites
 */
class CustomerInfo extends Model
{
    protected $table = 'customers_info';

    protected $fillable = [
        'customer_id', 'kpi_sku_count', 'kpi_sku_price_sum', 'kpi_order_count', 'kpi_shipment_count',
        'kpi_delivered_count', 'kpi_delivered_sum', 'kpi_refunded_count', 'kpi_refunded_sum', 'kpi_canceled_count',
        'kpi_canceled_sum',
    ];

    public static function factory(): CustomerInfoFactory
    {
        return CustomerInfoFactory::new();
    }
}
