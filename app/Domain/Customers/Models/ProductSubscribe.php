<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\ProductSubscribeFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для сущности "Подписка пользователя на товар"
 *
 * Class ProductSubscribe
 * @package App\Domain\Customers\Models
 *
 * @property int $id
 * @property int $customer_id - ид покупателя
 * @property int $product_id - ид покупателя
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class ProductSubscribe extends Model
{
    protected $table = 'product_subscribes';

    protected $fillable = ['customer_id', 'product_id'];

    public static function factory(): ProductSubscribeFactory
    {
        return ProductSubscribeFactory::new();
    }
}
