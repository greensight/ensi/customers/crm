<?php

namespace App\Domain\Kafka\Messages\Listen\Product;

use Illuminate\Support\Fluent;
use RdKafka\Message;

/**
 * @property array $dirty
 * @property ProductPayload $attributes
 * @property string $event
 */
class ProductEventMessage extends Fluent
{
    public const CREATE = 'create';
    public const UPDATE = 'update';
    public const DELETE = 'delete';

    public function __construct($attributes = [])
    {
        $attributes['attributes'] = new ProductPayload($attributes['attributes']);
        parent::__construct($attributes);
    }

    public static function makeFromRdKafka(Message $message): self
    {
        return new self(json_decode($message->payload, true));
    }
}
