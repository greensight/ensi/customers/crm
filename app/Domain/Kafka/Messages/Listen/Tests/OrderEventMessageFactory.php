<?php

namespace App\Domain\Kafka\Messages\Listen\Tests;

use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use Ensi\OmsClient\Dto\OrderStatusEnum;

class OrderEventMessageFactory extends EventMessageFactory
{
    protected function definition(): array
    {
        $event = $this->faker->randomElement([
            OrderEventMessage::CREATE,
            OrderEventMessage::UPDATE,
            OrderEventMessage::DELETE,
        ]);

        return [
            'event' => $event,
            'attributes' => [
                'id' => $this->faker->modelId(),
                'status' => $this->faker->randomElement([
                    OrderStatusEnum::WAIT_PAY,
                    OrderStatusEnum::NEW,
                    OrderStatusEnum::APPROVED,
                    OrderStatusEnum::DONE,
                    OrderStatusEnum::CANCELED,
                ]),
                'customer_id' => $this->faker->modelId(),
            ],
            'dirty' => $event == OrderEventMessage::UPDATE ? $this->faker->randomElements([
                'status',
            ]) : null,
        ];
    }
}
