<?php

namespace App\Domain\Kafka\Messages\Listen\Tests;

use Ensi\LaravelTestFactories\Factory;
use RdKafka\Message;

class EventMessageFactory extends Factory
{
    protected function definition(): array
    {
        return [];
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }

    protected function mergeDefinitionWithExtra(array $extra): array
    {
        $extraAttributes = $extra['attributes'] ?? [];
        unset($extra['attributes']);
        $array = parent::mergeDefinitionWithExtra($extra);

        $array['attributes'] = array_merge($array['attributes'], $extraAttributes);

        return $array;
    }
}
