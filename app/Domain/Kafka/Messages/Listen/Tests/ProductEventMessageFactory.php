<?php

namespace App\Domain\Kafka\Messages\Listen\Tests;

use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;

class ProductEventMessageFactory extends EventMessageFactory
{
    protected function definition(): array
    {
        return [
            'event' => OrderEventMessage::DELETE,
            'attributes' => [
                'id' => $this->faker->modelId(),
            ],
        ];
    }
}
