<?php

use App\Domain\Customers\Models\Favorite;
use App\Domain\Kafka\Actions\Listen\ListenProductAction;
use App\Domain\Kafka\Messages\Listen\Tests\ProductEventMessageFactory;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'favorites');

test("Action ListenProductAction success", function () {
    /** @var IntegrationTestCase $this */

    $productId_1 = 1;
    $productId_2 = 2;

    // Ensure favorites with received product_id are deleted
    /** @var Favorite $favorite_1 */
    Favorite::factory()->count(2)->create(['product_id' => $productId_1]);
    // Ensure other favorites are not affected
    /** @var Favorite $favorite_2 */
    Favorite::factory()->count(2)->create(['product_id' => $productId_2]);

    $message = ProductEventMessageFactory::new()->make([
        'attributes' => [
            'id' => $productId_1,
        ],
    ]);

    resolve(ListenProductAction::class)->execute($message);

    assertDatabaseMissing(Favorite::class, [
        'product_id' => $productId_1,
    ]);

    assertDatabaseHas(Favorite::class, [
        'product_id' => $productId_2,
    ]);
});
