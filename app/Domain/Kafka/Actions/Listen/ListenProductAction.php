<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Customers\Actions\Favorites\DeleteFavoritesAction;
use App\Domain\Kafka\Messages\Listen\Product\ProductEventMessage;
use RdKafka\Message;

class ListenProductAction
{
    public function __construct(protected DeleteFavoritesAction $action)
    {
    }

    public function execute(Message $message): void
    {
        $eventMessage = ProductEventMessage::makeFromRdKafka($message);
        if ($eventMessage->event !== ProductEventMessage::DELETE) {
            return;
        }

        $this->action->execute(productIds: [$eventMessage->attributes->id]);
    }
}
