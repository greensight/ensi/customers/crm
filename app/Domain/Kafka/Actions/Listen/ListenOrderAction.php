<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Customers\Models\Preference;
use App\Domain\Kafka\Messages\Listen\Order\OrderEventMessage;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Ensi\CustomersClient\Dto\RequestBodyPagination;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;
use RdKafka\Message;

class ListenOrderAction
{
    public function __construct(protected OrdersApi $ordersApi, protected ProductsApi $productsApi)
    {
    }

    /**
     * @throws \Ensi\OmsClient\ApiException
     * @throws \Ensi\PimClient\ApiException
     */
    public function execute(Message $message): void
    {
        $eventMessage = OrderEventMessage::makeFromRdKafka($message);
        if (in_array($eventMessage->event, [OrderEventMessage::UPDATE, OrderEventMessage::CREATE]) &&
            $eventMessage->attributes->status === OrderStatusEnum::DONE
        ) {
            try {
                $attributes = config('preferences');
                $products = collect(
                    $this->ordersApi->getOrder($eventMessage->attributes->id, 'items')->getData()->getItems()
                )->keyBy->getOfferId();
                $productsInfo = $this->collectProductsInfo(
                    $products->keys()->all(),
                    array_keys($attributes['including_attributes'])
                );
                foreach ($attributes['including_attributes'] as $attribute => $field) {
                    $this->createOrUpdatePreferences($products, $productsInfo, $eventMessage, $attribute, $field);
                }
                foreach ($attributes['attributes'] as $attribute) {
                    $this->createOrUpdatePreferences($products, $productsInfo, $eventMessage, $attribute);
                }
            } catch (\Throwable $e) {
                logger()->error("Error consume: {$e->getMessage()}", $e->getTrace());
            }
        }
    }

    /**
     * @throws \Ensi\PimClient\ApiException
     */
    protected function collectProductsInfo(array $productIds, array $include = []): Collection
    {
        $products = collect();
        do {
            $response = $this->productsApi->searchProducts(new SearchProductsRequest([
                'pagination' => new RequestBodyPagination(['type' => PaginationTypeEnum::CURSOR->value, 'cursor' => $cursor ?? null]),
                'include' => $include,
                'filter' => (object)['id' => $productIds],
            ]));
            $cursor = $response->getMeta()->getPagination()->getNextCursor();

            $products = $products->concat($response->getData());
        } while (!is_null($cursor));

        return $products;
    }

    protected function createOrUpdatePreferences(
        Collection        $orderProducts,
        Collection        $productsInfo,
        OrderEventMessage $eventMessage,
        $attribute,
        $field = null
    ): void {
        $productsInfo->groupBy(is_null($field) ? $attribute : "$attribute.$field")->each(
            function (Collection $productGroup, $value) use ($eventMessage, $attribute, $orderProducts) {
                /** @var Preference|null $preference */
                $preference = Preference::query()->where([
                    'customer_id' => $eventMessage->attributes->customer_id,
                    'attribute_name' => (string)$attribute,
                    'attribute_value' => (string)$value,
                ])->first();
                $groupProductCount = $productGroup->reduce(
                    fn ($carry, Product $product) => $carry + $orderProducts->get($product->getId())?->getQty(),
                    0
                );
                $groupProductSum = $productGroup->reduce(
                    fn ($carry, Product $product) => $carry + $orderProducts->get($product->getId())?->getPrice(),
                    0
                );
                Preference::query()->updateOrCreate(
                    [
                        'customer_id' => $eventMessage->attributes->customer_id,
                        'attribute_name' => (string)$attribute,
                        'attribute_value' => (string)$value,
                    ],
                    [
                        'product_count' => is_null($preference) ? $groupProductCount : $preference->product_count + $groupProductCount,
                        'product_sum' => is_null($preference) ? $groupProductSum : $preference->product_sum + $groupProductSum,
                    ]
                );
            }
        );
    }
}
