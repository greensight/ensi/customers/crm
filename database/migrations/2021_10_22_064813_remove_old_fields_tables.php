<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('customer_addresses');
        Schema::dropIfExists('customer_ya_cards');
        Schema::table('customer_favorites', function (Blueprint $table) {
            $table->dropConstrainedForeignId('customer_id');
        });
        Schema::dropIfExists('customers');

        Schema::create('customers_info', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned()->unique();

            $table->integer('kpi_sku_count')->unsigned()->nullable();
            $table->bigInteger('kpi_sku_price_sum')->unsigned()->nullable();
            $table->integer('kpi_order_count')->unsigned()->nullable();
            $table->integer('kpi_shipment_count')->unsigned()->nullable();
            $table->integer('kpi_delivered_count')->unsigned()->nullable();
            $table->bigInteger('kpi_delivered_sum')->unsigned()->nullable();
            $table->integer('kpi_refunded_count')->unsigned()->nullable();
            $table->bigInteger('kpi_refunded_sum')->unsigned()->nullable();
            $table->integer('kpi_canceled_count')->unsigned()->nullable();
            $table->bigInteger('kpi_canceled_sum')->unsigned()->nullable();

            $table->timestamps();
        });
        Schema::table('customer_favorites', function (Blueprint $table) {
            $table->bigInteger('customer_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers_info');
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('user_id')->unsigned()->unique();
            $table->integer('status')->unsigned();
            $table->text('comment_internal')->nullable();
            $table->bigInteger('manager_id')->unsigned()->nullable();
            $table->string('avatar')->nullable();
            $table->date('birthday')->nullable();
            $table->integer('gender')->nullable();
            $table->string('comment_status')->nullable();

            $table->string('legal_info_company_name')->nullable();
            $table->string('legal_info_company_address')->nullable();
            $table->string('legal_info_inn')->nullable();
            $table->string('legal_info_payment_account')->nullable();
            $table->string('legal_info_bik')->nullable();
            $table->string('legal_info_bank')->nullable();
            $table->string('legal_info_bank_correspondent_account')->nullable();
            $table->string('city')->nullable();

            $table->timestamps();
        });

        Schema::table('customer_favorites', function (Blueprint $table) {
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });
        Schema::create('customer_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id')->unsigned();

            $table->json('address');
            $table->boolean('default')->default(false);

            $table->timestamps();

            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->onDelete('cascade');
        });
        Schema::create('customer_ya_cards', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('customer_id')->unsigned();

            $table->string('card_panmask');
            $table->string('card_synonim');
            $table->string('card_country_code');
            $table->string('card_type');
            $table->string('ya_account_number');

            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
        });
    }
};
