<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('customer_id');

            $table->bigInteger('order_number')->unsigned()->nullable();
            $table->double('bonus_amount');
            $table->string('comment')->nullable();
            $table->timestamp('activation_date', 6)->nullable();
            $table->timestamp('expiration_date', 6)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonus_operations');
    }
};
