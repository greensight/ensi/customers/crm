<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        DB::table('customers')->update(['avatar' => null]);
        Schema::table('customers', function (Blueprint $table) {
            $table->string('avatar')->nullable()->change();
        });
    }

    public function down()
    {
        DB::table('customers')->update(['avatar' => null]);
        DB::statement('alter table customers alter column avatar type jsonb using avatar::jsonb');
    }
};
