<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->string('legal_info_company_name')->nullable();
            $table->string('legal_info_company_address')->nullable();
            $table->string('legal_info_inn')->nullable();
            $table->string('legal_info_payment_account')->nullable();
            $table->string('legal_info_bik')->nullable();
            $table->string('legal_info_bank')->nullable();
            $table->string('legal_info_bank_correspondent_account')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('legal_info_company_name');
            $table->dropColumn('legal_info_company_address');
            $table->dropColumn('legal_info_inn');
            $table->dropColumn('legal_info_payment_account');
            $table->dropColumn('legal_info_bik');
            $table->dropColumn('legal_info_bank');
            $table->dropColumn('legal_info_bank_correspondent_account');
        });
    }
};
