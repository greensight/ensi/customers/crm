<?php

namespace Tests;

use Ensi\CustomerAuthClient\Api\UsersApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Customers ===============

    // region service Customer Auth
    protected function mockCustomerAuthUsersApi(): MockInterface|UsersApi
    {
        return $this->mock(UsersApi::class);
    }
    // endregion
}
