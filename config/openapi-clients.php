<?php

return [
    'catalog' => [
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . "/api/v1",
        ],
    ],
   'customers' => [
      'customer-auth' => [
         'base_uri' => env('CUSTOMERS_CUSTOMER_AUTH_HOST') . "/api/v1",
      ],
      'customers' => [
         'base_uri' => env('CUSTOMERS_CUSTOMERS_HOST') . "/api/v1",
      ],
   ],
   'orders' => [
      'oms' => [
         'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . "/api/v1",
      ],
   ],
];
